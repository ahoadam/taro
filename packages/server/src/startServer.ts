import "reflect-metadata";

import { GraphQLServer } from 'graphql-yoga'
import { fileLoader, mergeTypes, mergeResolvers } from 'merge-graphql-schemas';
import { makeExecutableSchema } from "graphql-tools";
import * as session from 'express-session';
import * as path from 'path';

import {getConnectionOptions, createConnection} from "typeorm";

const port = 8000;

const createTypeormConn = async () => {
  const connectionOptions = await getConnectionOptions(process.env.NODE_ENV);
  return createConnection({ ...connectionOptions, name: "default" });
};

export const startServer = async () => {
  // gather all of the graphql assets

  const typesArray = fileLoader(path.join(__dirname, "./**/*.graphql"));
  const resolversArray = fileLoader(path.join(__dirname, "./**/*.resolvers.*"));
  
  const typeDefs = mergeTypes(typesArray);
  const resolvers = mergeResolvers(resolversArray);

  const schema = makeExecutableSchema({
    typeDefs,
    resolvers
  });
  
  const server = new GraphQLServer({
    schema,
    context: ({ request }) => ({
      session: request.session,
      req: request
    })
  });

  // Configure sessions
  server.express.use(
    session({
      secret: 'adam-is-the-greatest',
      name: 'testing',
      resave: false,
      saveUninitialized: false,
      cookie: {
        httpOnly: true,
        secure: false, // change to use node env variable
        maxAge: 60000
      }
    }) as any
  );

  const cors = {
    credentials: true,
    origin: 'http://localhost:3000'
  };

  // create connection to the db via typeorm
  await createTypeormConn();

  const app = await server.start({ cors, port });

  console.log(`app started on port ${port}`);

  return app;
}
