import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  OneToOne
} from "typeorm";

import {
  User
} from './User';

@Entity()
export class Profile extends BaseEntity {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  // @Column()
  // photo: string;

  @Column()
  name: string;

  @Column('int')
  age: number;

  @Column()
  gender: string;

  @Column()
  intent: string;

  @Column('text')
  bio: string;

  @OneToOne(type => User, user => user.profile, { eager: true })
  user: User;
}