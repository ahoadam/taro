import * as brcrypt from 'bcryptjs';

import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  BeforeInsert,
  OneToOne,
  JoinColumn
} from "typeorm";

import { Profile } from './Profile';

@Entity()
export class User extends BaseEntity {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  email: string;

  @Column()
  password: string;
  
  @OneToOne(type => Profile)
  @JoinColumn()
  profile: Profile;

  @BeforeInsert()
  async hasPasswordBeforeInsert() {
    this.password = await brcrypt.hash(this.password, 10);
  }
}