import * as yup from 'yup';
import { User } from '../../../entity/User';
import { formatYupError } from '../../../utils/formatYupError';

const schema = yup.object().shape({
  email: yup
    .string()
    .min(3, 'email not long enough')
    .max(255)
    .email('email is not valid'),
  password: yup
    .string()
    .min(8, 'password must be at least 8 characters long')
    .max(255)
});

export default {
  Mutation: {
    register: async (parent: any, args: any, context: any) => {
      // validate the user params
      try {
        await schema.validate(args, { abortEarly: false });
      } catch(err) {
        return formatYupError(err);
      }

      const {
        email,
        password,
        passwordConfirmation
      } = args;
      
      // Check if user already exists
      const userAlreadyExists = await User.findOne({ where: { email }});

      if (userAlreadyExists) {
        return [{
          path: 'email',
          message: 'user already exists'
        }];
      }

      // Check if the passwords match
      const passwordsMatch = password === passwordConfirmation;

      if (!passwordsMatch) {
        return [{
          path: 'password',
          message: 'passwords do not match'
        }];
      }
  
      const user = User.create({ email, password });

      await user.save();

      return null;
    }
  }
}
