import * as bcrypt from 'bcryptjs';
import { User } from '../../../entity/User';

const errorMessage = {
  path: 'login',
  message: 'username or password incorrect'
};

export default {
  Mutation: {
    login: async (parent: any, { email, password }: any, { session, req }: any) => {

      const {
        sessionID
      } = req;

      // get user they are trying to sign in as
      const user = await User.findOne({ where: { email }});

      if (!user) {
        return errorMessage;
      }

      // confirm they have enterred the correct password
      const valid = await bcrypt.compare(password, user.password);

      if (!valid) {
        return errorMessage;
      }

      // login successful
      session.userId = user.id;
      if (sessionID) {
        await session.save()
      }

      return null;
    }
  }
}