import { Profile } from '../../../entity/Profile';
import { User } from '../../../entity/User';

export default {
  Query: {
    getProfile: async (parent: any, args: any, { session, req }: any) => {

      const userId = session.userId;

      const user = await User.findOne({ where: { id: userId }});

      console.log(user);


    }
  },
  Mutation: {
    createProfile: async (parent: any, args: any, { session, req }: any) => {

      const {
        name, 
        age, 
        gender,
        intent,
        bio
      } = args;

      const userId = session.userId;
      const user = await User.findOne({ where: { id: userId } });

      // check and make sure there is a session available for this update.
      if (!userId) {
        return {
          path: 'profile',
          message: 'Please Login to update profile.'
        }
      }

      console.log(userId);

      const profile = await Profile.create({ name, age, gender, intent, bio, user: userId });

      await profile.save();

      return null;
    }
  }
}