import { combineReducers, Reducer } from 'redux';
// import { routerReducer } from 'react-router-redux';

// Import your state types and reducers here.
import { HelloState } from './hello/types';
import helloReducer from './hello/reducer';

// The top-level state object
export interface ApplicationState {
  hello: HelloState
}

export const initialState = {
  hello: {
    languageName: 'Typescript',
    enthusiasmLevel: 1
  }
}

// Whenever an action is dispatched, Redux will update each top-level application state property
// using the reducer with the matching name. It's important that the names match exactly, and that
// the reducer acts on the corresponding ApplicationState property type.
export const reducers: Reducer<ApplicationState> = combineReducers<ApplicationState>({
  // router: routerReducer,
  hello: helloReducer
});