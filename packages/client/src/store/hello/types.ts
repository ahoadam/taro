export interface HelloState {
  languageName: string;
  enthusiasmLevel: number;
}

export const enum HelloActionTypes {
  DECREMENT = '@@hello/DECREMENT',
  INCREMENT = '@@hello/INCREMENT',
}