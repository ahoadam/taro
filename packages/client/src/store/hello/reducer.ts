import { Reducer } from 'redux';
import { HelloState, HelloActionTypes } from './types';

const initialState: HelloState = {
  languageName: 'Typescript',
  enthusiasmLevel: 1
};

const reducer: Reducer<HelloState> = (state: HelloState = initialState, action) => {
  // We'll augment the action type on the switch case to make sure we have
  // all the cases handled.
  switch (action.type) {
    case HelloActionTypes.INCREMENT:
      return { ...state, enthusiasmLevel: state.enthusiasmLevel + 1 };
    case HelloActionTypes.DECREMENT:
      return { ...state, enthusiasmLevel: Math.max(1, state.enthusiasmLevel - 1) };
  }
  return state;
};

export default reducer;