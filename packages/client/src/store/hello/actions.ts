import { action } from 'typesafe-actions'
import { HelloActionTypes } from './types';

export const incrementEnthusiasm = () => action(HelloActionTypes.INCREMENT);
export const decrementEnthusiasm = () => action(HelloActionTypes.DECREMENT);

