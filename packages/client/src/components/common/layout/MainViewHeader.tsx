import * as React from 'react';

import {
  History
} from 'history';

import Menu from './menu/Menu';

import './MainViewHeader.less';


interface MainViewHeaderProps {
  children?: React.ReactNode;
  history: History
}

class MainViewHeader extends React.Component<MainViewHeaderProps> {
  render() {
    const {
      history
    } = this.props;

    return (
      <div className="main-view-header">
        <Menu history={history} />
        {this.props.children}
      </div>
    );
  }
}

export default MainViewHeader;
