import * as React from 'react';

import {
  Layout
} from 'antd';

import './MainViewLayout.less';

interface MainViewLayoutProps {
  title: React.ReactNode | string;
  header: React.ReactNode;
  content: React.ReactNode;
}

class MainViewLayout extends React.PureComponent<MainViewLayoutProps>{
  render() {
    const {
      title,
      header,
      content
    } = this.props;

    return (
      <Layout className="main-view-layout">
        <Layout.Header>
          {header}
        </Layout.Header>
        <h1 className={"main-view-layout-title"}>{title}</h1>
        <Layout.Content>
          {content}
        </Layout.Content>
      </Layout>
    )
  }
}

export default MainViewLayout;