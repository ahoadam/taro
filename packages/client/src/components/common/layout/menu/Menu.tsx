import * as React from 'react';

import {
  History
} from 'history';

import MenuTitle from './MenuTitle';

import {
  Drawer,
  Icon
} from 'antd';

import './Menu.less';

interface MenuState {
  visible: boolean;
}

interface MenuProps {
  history: History;
}

const MENU_ITEM_MAP = [
  {
    title: 'Search',
    avatar: <Icon type="search" theme="outlined" />,
    route: '/search',
    key: 'search'
  },
  {
    title: 'Browse',
    avatar: <Icon type="global" theme="outlined" />,
    route: '/browse',
    key: 'browse'
  },
  {
    title: 'Your Events',
    avatar: <Icon type="radar-chart" theme="outlined" />,
    route: '/your-events',
    key: 'your-events'
  },
  {
    title: 'Messages',
    avatar: <Icon type="message" theme="outlined" />,
    route: '/messages',
    key: 'messages'
  }
];

class Menu extends React.PureComponent<MenuProps, MenuState> {

  state = {
    visible: false
  }

  handleDrawerState = () => {
    const {
      visible
    } = this.state;

    this.setState({ visible: !visible });
  }


  onMenuItemClick = (route: string) => {
    const {
      history
    } = this.props;

    history.push(route)
  }

  renderMenuItems = () => {
    return MENU_ITEM_MAP.map((item) => {
      return (
        <div
          onClick={this.onMenuItemClick.bind(this, item.route)}
          key={item.key}
          className="menu-item"
        >
          <div className="menu-item-avatar">{item.avatar}</div>
          <div className="menu-item-title">{item.title}</div>
        </div>
      );
    });
  }

  render() {
    const {
      visible
    } = this.state;

    return (
      <div className="menu">
        <div className="menu-trigger" onClick={this.handleDrawerState}>
          <Icon type="menu-unfold" theme="outlined" />
        </div>
        <Drawer
          className="menu-drawer"
          title={<MenuTitle />}
          placement="left"
          closable={false}
          onClose={this.handleDrawerState}
          visible={visible}
        >
          {this.renderMenuItems()}
        </Drawer>
      </div>
    );
  }
}

export default Menu;
