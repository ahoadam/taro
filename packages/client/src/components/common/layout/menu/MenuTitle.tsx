import * as React from 'react';

import {
  Icon
} from 'antd';

import './MenuTitle.less';

class MenuTitle extends React.Component {
  render() {
    return (
      <div className="main-menu-title">
        <div className="main-menu-title-avatar">
          <Icon type="user" theme="outlined" />
        </div>
        <div className="main-menu-title-username">
          adamaho
        </div>
        <div className="main-menu-title-location">
          Toronto, ON
        </div>
        <div className="main-menu-title-stats">
          <div className="main-menu-title-stat">
            <div>
              101
            </div>
            <div className="main-menu-title-stat-desc">
              following
            </div>
          </div>
          <div className="main-menu-title-stat">
            <div>
              200
            </div>
            <div className="main-menu-title-stat-desc">
              followers
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default MenuTitle;
