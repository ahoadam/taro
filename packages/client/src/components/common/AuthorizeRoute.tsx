import * as React from 'react';

import {
  Route
} from 'react-router-dom';

class AuthorizeRoute extends React.Component {
  render() {
    const authorized = true;

    if (authorized) {
      return (
        <Route {...this.props} />
      );
    } else {
      return <div>login</div>;
    }
  }
}

export default AuthorizeRoute;
