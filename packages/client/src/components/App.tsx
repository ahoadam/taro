import * as React from 'react';

import {
  Switch,
  Route
} from 'react-router-dom';

import {
  Login,
  Register,
  Profile
} from '../modules/user';

import './App.less';

class App extends React.Component {
  render() {
    return (
      <div className="app">
        <Switch>
          {/* <Route path={'/'} exact={true} render={this.test} /> */},
          <Route path={'/profile'} exact={true} component={Profile} />
          <Route path={'/login'} exact={true} component={Login} />
          <Route path={'/register'} exact={true} component={Register} />
        </Switch>
      </div>
    );
  }
}

export default App;
