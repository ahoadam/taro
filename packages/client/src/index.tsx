import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { IntlProvider } from 'react-intl';
import { LocaleProvider } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';

import App from './components/App';
import registerServiceWorker from './registerServiceWorker';

import {
  BrowserRouter as Router
} from 'react-router-dom'

import { createStore } from 'redux';
import { ApplicationState, reducers } from './store';

// import Hello from './containers/Hello';
import { Provider } from 'react-redux';

import ApolloClient from 'apollo-boost';
import {
  ApolloProvider
} from 'react-apollo';

import './less/index.less';


const client = new ApolloClient({
  uri: 'http://localhost:8000',
  credentials: 'include'
});

const initialState = window.initialReduxState;

const store = createStore<ApplicationState, any, any, any>(reducers, initialState);

ReactDOM.render(
  <ApolloProvider client={client}>
    <IntlProvider locale="en">
      <LocaleProvider locale={enUS}>
        <Provider store={store}>
          <Router>
            <App />
          </Router>
        </Provider>
      </LocaleProvider>
    </IntlProvider>
  </ApolloProvider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
