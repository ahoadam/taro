import Login from './login/Login';
import Register from './register/Register';
import Profile from './profile/Profile';

export {
  Login,
  Register,
  Profile
};
