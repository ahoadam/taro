import * as React from 'react';
import gql from "graphql-tag";
import { Mutation } from "react-apollo";

import { RouteComponentProps } from 'react-router-dom';

import {
  Form,
  Input,
  Button
} from 'antd';

import {
  injectIntl,
  FormattedMessage,
  InjectedIntlProps
} from 'react-intl';

import {
  userMessages
} from '../userMessages';

import {
  FormComponentProps
} from 'antd/lib/form';

import './Register.less';

interface RegisterMutation {
  register: boolean;
}

interface RegisterFormProps extends FormComponentProps, RouteComponentProps, InjectedIntlProps {
  email: string;
  password: string;
  passwordConfirmation: string;
}

const REGISTER = gql`
  mutation registerUser($email: String!, $password: String!, $passwordConfirmation: String!) {
    register(email: $email, password: $password, passwordConfirmation: $passwordConfirmation) {
      path,
      message
    }
  }
`;

class Register extends React.PureComponent<RegisterFormProps> {

  handleSubmit = (e: any, mutate: any) => {
    e.preventDefault();

    const {
      form: { validateFields },
      history
    } = this.props;

    validateFields(async (err: object, fields: object) => {
      if (!err) {
        const {
          data: { register }
        } = await mutate({ variables: fields });

        if (register !== null) {
          console.log('handle errors');
        } else {
          history.push('/login');
        }
      }
    });
  }

  render() {
    const {
      form: { getFieldDecorator },
      intl: { formatMessage }
    } = this.props;

    return (
      <Mutation<RegisterMutation, RegisterFormProps>
        mutation={REGISTER}
      >
        {(mutate) => (
          <div className="login-form">
            <div className="login-form-container">
              <h1>EVNT</h1>
              <div className="login-form-title"><FormattedMessage {...userMessages.SIGN_UP} /></div>
              <Form onSubmit={(e: any) => this.handleSubmit(e, mutate)}>
              <Form.Item>
                  {getFieldDecorator('email', {})(
                    <Input
                      placeholder={formatMessage(userMessages.EMAIL_PLACEHOLDER)}
                    />
                  )}
                </Form.Item>
                <Form.Item>
                  {getFieldDecorator('password', {})(
                    <Input
                      type="password"
                      placeholder={formatMessage(userMessages.PASSWORD_PLACEHOLDER)}
                    />
                  )}
                </Form.Item>
                <Form.Item>
                  {getFieldDecorator('passwordConfirmation', {})(
                    <Input
                      type="password"
                      placeholder={formatMessage(userMessages.PASSWORD_CONFIRMATION_PLACEHOLDER)}
                    />
                  )}
                </Form.Item>
                <Form.Item>
                  <Button
                    type="primary"
                    htmlType="submit"
                  >
                    <FormattedMessage {...userMessages.SIGN_UP} />
                  </Button>
                </Form.Item>
              </Form>
            </div>
          </div>
        )}
      </Mutation>
    );
  }
}

export default Form.create()(injectIntl(Register));
