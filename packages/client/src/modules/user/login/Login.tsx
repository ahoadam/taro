import * as React from 'react';
import gql from "graphql-tag";
import { Mutation } from "react-apollo";

import { RouteComponentProps } from 'react-router-dom';

import {
  Form,
  Input,
  Button
} from 'antd';

import {
  injectIntl,
  FormattedMessage,
  InjectedIntlProps
} from 'react-intl';

import {
  userMessages
} from '../userMessages';

import {
  FormComponentProps
} from 'antd/lib/form';

import './Login.less';

interface LoginMutation {
  register: boolean;
}

interface LoginProps extends FormComponentProps, RouteComponentProps, InjectedIntlProps {
  email: string;
  password: string
}

const LOGIN = gql`
  mutation loginMutation($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      path,
      message
    }
  }
`;

class Login extends React.PureComponent<LoginProps, any> {

  handleSubmit = (e: any, mutate: any) => {
    e.preventDefault();

    const {
      form: { validateFields },
      history
    } = this.props;

    validateFields(async (err: object, fields: object) => {
      if (!err) {
        const {
          data: { login }
        } = await mutate({
          variables: fields
        });

        if (login !== null) {
          console.log('handle errors');
        } else {
          history.push('/profile');
        }
      }
    });
  }

  render() {
    const {
      form: { getFieldDecorator },
      intl: { formatMessage }
    } = this.props;

    return (
      <Mutation<LoginMutation, LoginProps> 
        mutation={LOGIN}
      >
        {(mutate) => (
          <div className="login-form">
            <div className="login-form-container">
              <h1>EVNT</h1>
              <div className="login-form-title"><FormattedMessage {...userMessages.SIGN_IN} /></div>
              <Form onSubmit={(e: any) => this.handleSubmit(e, mutate)}>
                <Form.Item>
                  {getFieldDecorator('email', {})(
                    <Input placeholder={formatMessage(userMessages.EMAIL_PLACEHOLDER)} />
                  )}
                </Form.Item>
                <Form.Item>
                  {getFieldDecorator('password', {})(
                    <Input type="password" placeholder={formatMessage(userMessages.PASSWORD_PLACEHOLDER)} />
                  )}
                </Form.Item>
                <Form.Item>
                  <Button
                    type="primary"
                    htmlType="submit"
                  >
                    <FormattedMessage {...userMessages.SIGN_IN} />
                  </Button>
                </Form.Item>
              </Form>
            </div>
          </div>
        )}
      </Mutation>
    );
  }
}

export default Form.create()(injectIntl(Login));