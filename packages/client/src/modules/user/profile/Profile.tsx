import * as React from 'react';

import MainViewLayout from '../../../components/common/layout/MainViewLayout';
import MainViewHeader from '../../../components/common/layout/MainViewHeader';
import { RouteComponentProps } from 'react-router-dom';

class Profile extends React.PureComponent<RouteComponentProps> {
  render() {
    const {
      history
    } = this.props;

    return (
      <MainViewLayout
        title={'Profile'}
        header={<MainViewHeader history={history} />}
        content={<div>content</div>}
      />
    );
  }
}

export default Profile;
