import {
  defineMessages
} from 'react-intl';

export const userMessages = defineMessages({
  EMAIL_PLACEHOLDER: {
    id: 'User.Email.Placeholder',
    defaultMessage: 'Email'
  },
  PASSWORD_PLACEHOLDER: {
    id: 'User.Password.Placeholder',
    defaultMessage: 'Password'
  },
  PASSWORD_CONFIRMATION_PLACEHOLDER: {
    id: 'User.PasswordConfirmation.Placeholder',
    defaultMessage: 'Confirm Password'
  },
  SIGN_IN: {
    id: 'User.SignIn.',
    defaultMessage: 'Sign In'
  },
  SIGN_UP: {
    id: 'User.SignUp',
    defaultMessage: 'Sign Up'
  }
})