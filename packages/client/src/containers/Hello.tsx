import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import * as actions from '../store/hello/actions';
import { ApplicationState } from '../store/index';
import Hello from '../components/Hello';

export function mapStateToProps({ hello: { enthusiasmLevel, languageName } }: ApplicationState) {
  return {
    enthusiasmLevel,
    name: languageName,
  }
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onIncrement: () => dispatch(actions.incrementEnthusiasm()),
    onDecrement: () => dispatch(actions.decrementEnthusiasm()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Hello);